# opc-client-graph
This project is the client of the [server](https://gitlab.com/abs_portfolio/opc-ua-raspberry.pi/opc-server-graph). It consists of 2 main files. 
*  The `monitoring.js` which acts as the client to the opc server & as server for the html/cs/js html interface.
*  The `index.html` which is the visualization of the measurments.

Firstly make sure you have installed node-js on your system [here](https://nodejs.org/en/)

In order for the project to used we need to install the opc-ua node js framework provided by [node-opcua](https://github.com/node-opcua).

Installation of node-opcua is preatty staightforword :

```
    $ mkdir sample_client
    $ cd sample_client
    $ npm init                      # creates a package.json
    $ npm install node-opcua --save
    $ npm install async --save
```

The `monitoring.js` represents the client of the system. It is written on node-js and its creating a subscription to the  [server](https://gitlab.com/abs_portfolio/opc-ua-raspberry.pi/opc-server-graph).
With this subscription the client monitors the requested value and uppon change detection updates its measurment. 

The `monitoring.js`also acts as server for the `index.html`. It opens a socket connection in order to distribute the measurment obtained from the opc-server uppon request.

The index.html visualizes the measurment obtained from the `monitoring.js` with a simple chart.

*Screenshot of the system while active.*

![Home Scrn](./screenshot/viz.png)